package tech.tams.reminder;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import static tech.tams.reminder.RemindersDatabaseHelper.TABLE_NAME;

/**
 *          reminder
 *          v 0.17.5
 *          by D. Scott Boggs for the Association of Mad Scientists
 *
 *      This is a simple reminders application for Android. It is currently (May 11 2017) in pre -
 *      alpha stage, as it doesn't actually remind you of anything. I will release version 1.x when-
 *      ever that functionality is enabled, and further iterations up until 2.x will be bug fixes or
 *      minor modifications, with the version format 1.YY.M[a], with the final letter being an
 *      optional value for multiple iterations within a month.
 *
 *      Once version 1.x is released, I will begin work on version 2.x, which aims to support
 *      location-based reminders, as well as the ability to remind a contact (by scheduling an SMS
 *      or email). Repeatable reminders may be added in a minor revision to either v1.x or v2.x.
 *
 *      I am writing this application for a few reasons:
 *          1. I find it surprising that there isn't an open-source reminder application for Android
 *              already
 *          2. I want to be able to remind myself without telling Google that I'm reminding myself
 *          3. It's a relatively simple application that I can use to for education in the Android
 *              ecosystem.
 *          4. I really want the ability to remind myself when I return to my current location.
 *
 *      This application also aims to explain all of its actions  through either comments or self-
 *      explanatory code (i.e. using boolean successful = doAThing(); if(successful) instead of
 *      boolean b; b = doAThing(); if(b){}), and patches to make the code more readable as well as
 *      questions on specific functionality are more than welcome. This is, after all, more of an
 *      educational endeavor than an attempt to create a commercially viable android application.
 *
 *      This application is released as provided freely to users with the hope that it may be useful,
 *      however there is no guarantee that it will be useful or that it won't break everything. You
 *      are free to modify and use or use verbatim any section of this code, provided you also
 *      use this 'share-alike' license, although proprietary (closed) licenses can be purchased from
 *      the aforementioned Association by contacting sysadmin@tams.tech. For a more well-defined
 *      license than "don't be a money-grubbing bastard" refer to the GPLv3, as it has proven to be
 *      a benefit to many similar non-profit or other non-money-grubbing organizations.
 *
 *      Specifically, and most importantly, neither myself nor the Association of Mad Scientists can
 *      be held responsible if this application fails it it's efforts to remind you to do whatever
 *      it is that you're forgetting to do. Also, to any developers, any code which uses derivatives
 *      of this code must also be open-source, and credit must be given, unless you have explicit
 *      written permission otherwise.
 *
 */

public class MainActivity extends AppCompatActivity {
    RemindersDatabaseHelper reminders_db;
    FloatingActionButton fab;
    Intent add_reminder_intent;
    ListView listView;
    boolean alarmSet;
    private int jobId;
    private ComponentName notificationClass;
    public static final long ONE_SECOND = 1000;
    public static final long ONE_MINUTE = ONE_SECOND * 60;
    public static final long ONE_HOUR = ONE_MINUTE * 60;
    public static final long ONE_DAY = ONE_HOUR * 24;
    Intent rarIntent;
    PendingIntent rarPi;
    AlarmManager alarmManager;

    // these values are for working with the a specific reminder that is being scheduled or saved,
    // not for all of the reminders
    //private Integer hour, minute, priority;
    //private String description;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //init vars
        Toolbar toolbar     =   (Toolbar) findViewById(R.id.toolbar);
        add_reminder_intent =   new Intent(this,tech.tams.reminder.AddReminderActivity.class);
        listView            =   (ListView) findViewById(R.id.reminders_ListView);
        reminders_db        =   new RemindersDatabaseHelper(this);
        fab                 =   (FloatingActionButton) findViewById(R.id.fab);
        jobId               =   0;
        notificationClass   =   new ComponentName(this, ReminderAlertReceiver.class);

        //setParams
        setSupportActionBar(toolbar);
        listView.setEnabled(false);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Floating Action Button "fab" calls the intent to create a new reminder.
                startActivityForResult(add_reminder_intent,1);
            }
        });
        //refreshes the listview
        updateList();
    }

    /* onCreate calls this method after making sure that all objects have some default value. On
    first run, the database should be empty, so it won't diplay anything. Once a 'reminder' has been
    added to the database, it will show that in a listView, with each item having a design based on
    ~/res/layout/reminders_list_item.
     */
    private void updateList(){
        Cursor data = reminders_db.getListContents(); // this Cursor object is the link between the list and the db
        if(data.getCount()<1) {         //  if the database is empty
            listView.setEnabled(false); //  disable the list
            listView.setVisibility(View.INVISIBLE);
            // TODO: 4/18/17 alternative text view to say "no reminders yet created" or whatever
        }
        else{   // the database has at least one reminder in it
            listView.setEnabled(true);
            listView.setVisibility(View.VISIBLE);
            String [] fromColumns = {RemindersDatabaseHelper.COL_REMINDER_DESCRIPTION, RemindersDatabaseHelper.COL_PRIORITY};
            // this ^^ array contains the values needed to get the description and priority from the DB
            int[] toViews = {R.id.descriptionListText,R.id.priorityDisplay};
            // and this ^^ one has the places where those values need to be put
            SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, R.layout.reminders_list_item, data, fromColumns, toViews, 0);
            listView.setAdapter(adapter);
            // simple as that. (pretty sure I took this 'else' block verbatim from the Android documentation)
        }
    }

    /*  This method is called when there is an activity which was called from this activity which
    has provided a result. In this case, that should always be 'AddReminderActivity', although,
    I should verify that in the future in the code. This method should call the method to schedule
    the reminder, once that exists.
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent){
        Reminder rem = new Reminder();
        super.onActivityResult(requestCode,resultCode,intent);
        switch (requestCode){
            case 1:     // 1 is the request code used by the startActivityOnResult() call.
                switch (resultCode){
                    case RESULT_CANCELED:
                        // if the user presses the cancel button, this case is called. it does
                        // nothing
                        break;
                    case RESULT_OK:
                        // if the user presses the "add reminder" button, it calls this case.
                        rem.setDay(intent.getIntExtra("day",-1));
                        rem.setHour(intent.getIntExtra("hour",-1));
                        rem.setMinute(intent.getIntExtra("minute",-1));
                        rem.setDescription(intent.getStringExtra("description"));
                        rem.setPriority(intent.getIntExtra("priority",-1));
                        rem.setTimestamp(
                                SystemClock.elapsedRealtime() +
                                rem.getDay() * ONE_DAY +
                                rem.getHour() * ONE_HOUR +
                                rem.getMinute() * ONE_MINUTE
                        );
                        rem = addData(rem);   // adds the data that was just saved to the database
                        alarmSet = setAlarm(rem); //alarmSet is to check for success, it's a bool

                    }
                break;
        }
    }

    /*  TODO: this is the core component of the app that needs completed before the app can be considered functional!!!
    First, set up a JobScheduler for the defined date and time;
      OR
    Implement alarm manager. Both need to be done for backwards compatibility, but JobScheduler is the "modern"
    way of doing it.
     THEN
    either set the priority as an int and description as a string and include that in the scheduled intent
      OR
    pass a unique identifier (perhaps from the SQLite "id" column?) and have that be a reference to a database
    entry which houses that information.

    Either way a new intent needs to be called as well as another object (ScheduledJob perhaps?) created.
     */
    private boolean setAlarm(Reminder reminder) {
        boolean res = false;
        Long currentTime = System.currentTimeMillis();
        if(reminder.getDescription()!= null && reminder.getDescription().length() !=0) {
            if(reminder.getPriority()>-1&&reminder.getPriority()<=100){
                ReminderAlertReceiver rar = new ReminderAlertReceiver();
                registerReceiver(rar,new IntentFilter("tech.tams.reminder.ReminderAlertReceiver"));
                rarIntent = new Intent("tech.tams.reminder.ReminderAlertReceiver");
                //rarIntent.putExtra("priority", reminder.getPriority());
                //rarIntent.putExtra("description", reminder.getDescription());
                rarIntent.putExtra("dbid",reminder.getId());
                rarPi = PendingIntent.getBroadcast( this, 0,rarIntent,0);
                alarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
                alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, reminder.getTimeStamp(),rarPi);
            }
//            if (Build.VERSION.SDK_INT > 21){
//                // TODO: 4/19/17 jobScheduler Version for modernization
//            }

        }
        return res;
    }
    /*  Method addData
     Called directly from the onActivityResult() method, in the case that the "add reminder" button
     was pressed.
     */
    public Reminder addData(Reminder rem){
        boolean successful;
        long id;
        if(rem.getDescription() != null && rem.getDescription().length() !=0)  {  //checks to see if the description contains any text
            id = reminders_db.addReminder(rem.getContent());    // gets the appropriately formatted
                    // data from the Reminder object, and gives it to the database helper object
            successful = id != -1; //sets successful if the ID is not -1 (-1 means failure from the SQL command)
            if (successful){
                Toast.makeText(MainActivity.this,"Successfully saved entry in database",Toast.LENGTH_LONG).show();
                rem.setId(id);
                updateList();
                return rem;
            }
            else{
                Toast.makeText(MainActivity.this,"Saving entry in database has failed",Toast.LENGTH_LONG).show();
                return rem;
            }
        }
        else {
// TODO: 5/12/17 make this ^^ more transparent to the user. If this fails, it would not save a reminder and wouldn't say why
            return rem;
        }
    }

//<-----------------    everything after this point is auto-generated    -------------------------->

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_clear_database) {
            Toast.makeText(this,String.valueOf(reminders_db.getWritableDatabase().delete(TABLE_NAME, null, null) + " rows deleted"),Toast.LENGTH_SHORT).show();
            updateList();
            return true;
            // TODO: 5/26/17 cancel scheduled events
        }
        return super.onOptionsItemSelected(item);
    }
}
