package tech.tams.reminder;

import android.content.ContentValues;

import static java.lang.Integer.valueOf;
import static tech.tams.reminder.RemindersDatabaseHelper.COL_PRIORITY;
import static tech.tams.reminder.RemindersDatabaseHelper.COL_REMINDER_DESCRIPTION;
import static tech.tams.reminder.RemindersDatabaseHelper.COL_TIMESTAMP;

/**
 * Created by scott on 4/12/17.
 *
 * This is an object to store and work with a single instance of a reminder. standard getters and setters
 */

public class Reminder {
    private String description;
    private int priority,hour,minute,day;
    private long timestamp,id;
    public Reminder(){}

    public Reminder(long timestamp, String description, int priority) {
        this.timestamp=timestamp;
        this.description=description;
        this.priority=priority;
    }

    //getters
    public long getId(){                                return id;                                  }
    public String getDescription() {                    return description;                         }
    public int getPriority(){                           return priority;                            }
    public int getDay(){                                return day;                                 }
    public int getHour(){                               return hour;                                }
    public int getMinute(){                             return minute;                              }
    public long getTimeStamp(){                         return timestamp;                           }
    //setters
    public void setDay(int day){                        this.day=day;                               }
    public void setId(long id){                         this.id=id;                                 }
    public void setDescription(String description){     this.description = description;             }
    public void setPriority(int priority){              this.priority = priority;                   }
    public void setHour(int hour) {                     this.hour=hour;                             }
    public void setMinute(int minute) {                 this.minute=minute;                         }
    public void setTimestamp(long timestamp) {          this.timestamp=timestamp;                   }
    //  a sane way of outputting a string value of the reminder
    // todo more sane
    @Override
    public String toString(){
        String ret = hour+":"+minute+"@"+description+"@"+Integer.toString(priority);
        return ret;
    }

    //  the ContentValues object type is what is used to place the data into the database.
    public ContentValues getContent() {
        ContentValues vals = new ContentValues();
        vals.put(COL_TIMESTAMP,timestamp);
        vals.put(COL_REMINDER_DESCRIPTION,description);
        vals.put(COL_PRIORITY,priority);
        return vals;
    }
}
