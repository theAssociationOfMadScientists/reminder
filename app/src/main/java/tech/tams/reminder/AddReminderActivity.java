package tech.tams.reminder;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.SeekBar;
import android.widget.TimePicker;
import android.widget.Toast;

public class AddReminderActivity extends AppCompatActivity {

    /**
     * This activity is called when the floating action button is pressed to add a reminder to the
     * database. This activity is fully functional.
     *
     * Written by D. Scott Boggs in April 2017
     *
     */

    //  INIT vars
    Integer day, hour, minute, priority;
    String description;
    ListView remindersList;
    AutoCompleteTextView reminderDescription;
    EditText editDaysText,editHoursText,editMinutesText;
    SeekBar prioritySlider;
    Button btnAdd,btnCancel;
    PopupWindow timePickerWindow,datePickerWindow;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_reminder_view);

        context=this;
        // Set values for views
        remindersList       = (ListView)                findViewById(R.id.reminders_ListView);
        reminderDescription = (AutoCompleteTextView)    findViewById(R.id.description_input);
        btnAdd              = (Button)                  findViewById(R.id.add_rem_button);
        btnCancel           = (Button)                  findViewById(R.id.cancel_button);
        prioritySlider      = (SeekBar)                 findViewById(R.id.priority_slider);
        editDaysText        = (EditText)                findViewById(R.id.days_input);
        editHoursText       = (EditText)                findViewById(R.id.hours_input);
        editMinutesText     = (EditText)                findViewById(R.id.minutes_input);

        //getActionBar().setTitle(R.string.add_reminder_action_bar_title);
        getSupportActionBar().setTitle(R.string.add_reminder_action_bar_title);

        /*  When btnAdd is pressed, it finalizes the values in timePicker, prioritySlider, and
            reminderDescription, passing it back to the activity which called it (MainActivity)
            when it calls finish()
         */
        btnAdd.setOnClickListener(new View.OnClickListener(){
           @Override
            public void onClick(View v){

               // Gets all the values at the time the button is pressed

               try{
                   day = Integer.parseInt(String.valueOf(editDaysText.getText()));
               }
               catch (NumberFormatException e){
                   day = 0;
               }
                   /*
                   In the particular case that a value is not filled in, it's assumed the user wants
                   that value left as the default 0.
                    */
               try{
                   hour = Integer.parseInt(String.valueOf(editHoursText.getText()));
               }
               catch (NumberFormatException e){
                   day = 0;
               }
               try{
                   minute   = Integer.parseInt(String.valueOf(editMinutesText.getText()));
               }
               catch (NumberFormatException e){
                   minute = 0;
               }
               description = reminderDescription.getText().toString();
               if (description == ""){
                   Toast.makeText(context,"Please tell me what to remind you of!", Toast.LENGTH_LONG).show();
                   return;
               }
               priority = prioritySlider.getProgress();

               // and puts them to the result intent as Extras
               Intent intent = new Intent();
               intent.putExtra("day",day);
               intent.putExtra("hour",hour);
               intent.putExtra("minute",minute);
               intent.putExtra("description",description);
               intent.putExtra("priority",priority);
               // Setting the result is important, and you have to set up conditionals in the recvng
               //   activity based on the result code.
               setResult(RESULT_OK, intent);
               // finish() sends the intent.
               finish();
           }

        });
        /*  When btnCancel is pressed, it simply returns a result of "cancelled" to the activity
        which called it
         */
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }
}
