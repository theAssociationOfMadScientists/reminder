package tech.tams.reminder;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.widget.Toast;

/**
 * ReminderAlertReceiver will be the activity which is called by the JobScheduler, so it should
 *  handle various priority values and notify the user based on those values.
 *
 * Basically, TODO this whole thing.
 *
 */

public class ReminderAlertReceiver extends BroadcastReceiver {

    RemindersDatabaseHelper remindersDatabase;
    Reminder thisReminder;
    Intent receivedIntent;
    int priority;
    String description;
    long id;
    private static final int NOTIF_ID = 23;
    private static final String TAG_TIMESTAMP_DB = "timestamp_db";
    private static final String TAG_TIMESTAMP_SYS = "timestamp_sys";
    @Override
    public void onReceive(Context context, Intent intent) {
        receivedIntent = intent;
        id = receivedIntent.getLongExtra("dbid",-1);
        remindersDatabase = new RemindersDatabaseHelper(context);
        thisReminder=remindersDatabase.getReminder(id);
        priority = thisReminder.getPriority();
        description = thisReminder.getDescription();
        Log.d(TAG_TIMESTAMP_DB, String.valueOf(thisReminder.getTimeStamp()) + ", as received by the database");
        Log.d(TAG_TIMESTAMP_SYS, String.valueOf(SystemClock.elapsedRealtime())+", as received from the system clock");
        createNotification(context, description, priority);
        remindersDatabase.delete(id);
    }

    // TODO: 5/25/17 priority options
    private void createNotification(Context context, String description, int priority) {
        if (priority < 50 && priority > 0){
            Toast.makeText(context,description + " :|: " + String.valueOf(priority),Toast.LENGTH_LONG).show();
        }
        else{
            // TODO: 5/26/17 add sound
            NotificationCompat.Builder builder =
                    new NotificationCompat.Builder(context)
                    .setSmallIcon(R.drawable.notification_icon)
                    .setContentTitle("remember to...")
                    .setContentText(description);
            Intent resultIntent = new Intent (context, MainActivity.class);
            TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(context);
            taskStackBuilder.addParentStack(MainActivity.class);
            taskStackBuilder.addNextIntent(resultIntent);
            PendingIntent pendingIntent = taskStackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setAutoCancel(true);
            builder.setContentIntent(pendingIntent);
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(NOTIF_ID,builder.build());
        }
    }
}
