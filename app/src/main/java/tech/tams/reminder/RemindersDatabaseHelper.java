package tech.tams.reminder;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by scott on 4/15/17.
 *
 * This method was auto-generated from the SQLOpenHelper in the Android Studio and then modified to
 * suit my needs.
 */

public class RemindersDatabaseHelper extends SQLiteOpenHelper {

    //CONSTANTS
    public static final String DATABASE_NAME = "listOfReminders.db";
    public static final String TABLE_NAME = "list_of_reminders";
    public static final String COL_ID = "_id";
    public static final String COL_TIMESTAMP= "timestamp";
    public static final String COL_REMINDER_DESCRIPTION = "description";
    public static final String COL_PRIORITY = "priority";
    private static final String TAG_DELETE_ERROR = "del_err";

    /**
     * Create a helper object to create, open, and/or manage a database.
     * This method always returns very quickly.  The database is not actually
     * created or opened until one of {@link #getWritableDatabase} or
     * {@link #getReadableDatabase} is called.
     *
     * @param context to use to open or create the database
     *            not used:
     * //@param name    of the database file, or null for an in-memory database
     * //@param factory to use for creating cursor objects, or null for the default
     * //@param version number of the database (starting at 1); if the database is older,
     *                {@link #onUpgrade} will be used to upgrade the database; if the database is
     *                newer, {@link #onDowngrade} will be used to downgrade the database
     */
    public RemindersDatabaseHelper(Context context) {
        super(context, DATABASE_NAME,null,1);
    }


    /**
     * Called when the database is created for the first time. This is where the
     * creation of tables and the initial population of the tables should happen.
     *
     * @param db The database.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        //  This is a string with a standard SQL query
        String createTable = "CREATE TABLE " + TABLE_NAME + "( " +
                COL_ID  + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COL_TIMESTAMP + " INTEGER, " +
                COL_REMINDER_DESCRIPTION + " TEXT, " +
                COL_PRIORITY + " INTEGER);";
        //  which is then passed to the database object.
        db.execSQL(createTable);
    }

    /**
     * Called when the database needs to be upgraded. The implementation
     * should use this method to drop tables, add tables, or do anything else it
     * needs to upgrade to the new schema version.
     * <p>
     * <p>
     * The SQLite ALTER TABLE documentation can be found
     * <a href="http://sqlite.org/lang_altertable.html">here</a>. If you add new columns
     * you can use ALTER TABLE to insert them into a live table. If you rename or remove columns
     * you can use ALTER TABLE to rename the old table, then create the new table and then
     * populate the new table with the contents of the old table.
     * </p><p>
     * This method executes within a transaction.  If an exception is thrown, all changes
     * will automatically be rolled back.
     * </p>
     *
     * @param db         The database.
     * @param oldVersion The old database version.
     * @param newVersion The new database version.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }
    /*  method addReminder
    takes a ContentValues object and puts it in the database. This should have been called from
        MainActivity in the addData() method, and uses the ContentValues parameter generated in the
        Reminder object's getContent() method.
     */
    public long addReminder(ContentValues vals){
        SQLiteDatabase db = this.getWritableDatabase();
        long result = db.insert(TABLE_NAME, null, vals);
        return result;
    }
    public Reminder getReminder(long rowID){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME,
            null,
            COL_ID+"="+String.valueOf(rowID),
            null,null,null,null,null
        );
        cursor.moveToFirst();
        return new Reminder(
            //pass timestamp, descr, and priority for the row
            cursor.getInt(1),
            cursor.getString(2),
            cursor.getInt(3)
        );
    }
    public Cursor getListContents(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor data = db.rawQuery("SELECT * FROM " + TABLE_NAME,null);
        return data;
    }

    public int delete(long id) {
        SQLiteDatabase db = this.getWritableDatabase();
        int rowsDel = db.delete(TABLE_NAME,COL_ID+"="+String.valueOf(id),null);
        if (rowsDel == 0){
            Log.e(TAG_DELETE_ERROR,"Error deleting row ID " + String.valueOf(id) + " from the database. No entries deleted.");
        }
        else if (rowsDel > 1){
            Log.e(TAG_DELETE_ERROR,"Error deleting row ID " + String.valueOf(id) + " from the database. " + String.valueOf(rowsDel) + " entries deleted.");
        }
        return rowsDel;
    }
}
